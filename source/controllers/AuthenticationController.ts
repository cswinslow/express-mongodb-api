import { Request, Response } from 'express';
import { sign, verify } from 'jsonwebtoken';
import { Error } from 'mongoose';

import configuration from '../configuration';

import { ClientError } from '../exceptions/clientError';
import { NotFoundError } from '../exceptions/notFoundError';
import { UnauthorizedError } from '../exceptions/unauthorizedError';

import { processErrors } from '../utilities/processErrors';

import { User } from '../models/user';

class AuthenticationController {
    static login = async (request: Request, response: Response) => {
        let { username, password } = request.body;

        if (!(username && password)) {
            throw new ClientError("Username and password are required.");
        };

        const user = await User.findOne({ username: username }).exec();

        if (!user || !(await user.validateCredentials(password))) {
            throw new UnauthorizedError("Invalid password.");
        }

        // @ts-ignore
        const access_token = sign({ userId: user._id.toString(), role: user.role }, configuration.jwt.secret!, {
            expiresIn: '20m',
            notBefore: '0',
            algorithm: 'HS256',
            audience: configuration.jwt.audience,
            issuer: configuration.jwt.issuer,
            subject: 'Access'
        });

        // @ts-ignore
        const refresh_token = sign({ userId: user._id.toString() }, configuration.jwt.secret!, {
            expiresIn: '8h',
            notBefore: '0',
            algorithm: 'HS256',
            audience: configuration.jwt.audience,
            issuer: configuration.jwt.issuer,
            subject: 'Refresh'
        });

        response.type('json').send({ access_token: access_token, refresh_token: refresh_token });
    };

    static changePassword = async (request: Request, response: Response) => {
        const id = response.locals.jwtPayload.userId;
        const { oldPassword, newPassword } = request.body;

        if (!(oldPassword && newPassword)) {
            throw new ClientError("Passwords do not match.");
        };

        const user = await User.findById(id);

        if (!user) {
            throw new NotFoundError(`User with ID ${id} not found.`);
        } else if (!(await user.validateCredentials(oldPassword))) {
            throw new UnauthorizedError("Old password does not match.");
        }

        user.password = newPassword;

        try {
            await user.save();
        } catch (changeError) {
            console.error(changeError);

            const error = changeError as Error.ValidationError;

            throw new ClientError(processErrors(error));
        }

        response.type('json').status(204).send();
    };

    static refreshAccessToken = async (request: Request, response: Response) => {
        const token = <string>request.headers['authorization'];
        let data;

        try {
            data = <any>verify(token?.split(' ')[1], configuration.jwt.secret!, {
                complete: true,
                algorithms: ['HS256'],
                clockTolerance: 0,
                ignoreExpiration: false,
                ignoreNotBefore: false,
                audience: configuration.jwt.audience,
                issuer: configuration.jwt.issuer,
                subject: 'Refresh'
            });
        } catch (error) {
            response.status(401)
                .type('json')
                .send(JSON.stringify({ message: "Missing or invalid refresh token." }));

            return;
        }

        if (data) {
            const user = await User.findById(data.payload.userId);

            if (!user) {
                throw new NotFoundError(`User with ID ${data.payload.userId} not found.`);
            }

            // @ts-ignore
            const access_token = generateAccessToken({ userId: user._id.toString(), role: user.role });

            response.type('json').send({ access_token: access_token });
        } else {
            throw new UnauthorizedError("Refresh token was verified, but the data is unreadable. Please reauthenticate manually.");
        }
    };
};

export default AuthenticationController;
