import { Request, Response } from 'express';
import { Error } from 'mongoose';

import { ClientError } from '../exceptions/clientError';
import { NotFoundError } from '../exceptions/notFoundError';

import { processErrors } from '../utilities/processErrors';

import { User, IUser } from '../models/user';

class UserController {
    static getAll = async (request: Request, response: Response) => {
        const users = await User.find();

        response.status(200).type('json').send(users);
    };

    static getOne = async (request: Request, response: Response) => {
        const id: string = request.params.id;
        const user = await User.findById(id).exec();

        if (!user) {
            throw new NotFoundError(`User with ID ${id} not found.`);
        }

        response.status(200).type('json').send(user?.toJSON());
    };

    static createUser = async (request: Request, response: Response) => {
        const { email, firstName, isActive, lastName, username, password, role } = request.body;
        let user;

        try {
            user = User.buildUser({ email, firstName, isActive, lastName, username, password, role } as IUser);

            await user.save();
        } catch (createError: any) {
            console.error(createError);

            const error = createError as Error.ValidationError;

            throw new ClientError(processErrors(error));
        }

        response.status(201).type('json').send(user.toJSON());
    };

    static editUser = async (request: Request, response: Response) => {
        const id: string = request.params.id;
        const user = await User.findById(id).select(['_id', 'email', 'firstName', 'isActive', 'lastName', 'username', 'role']);

        if (!user) {
            throw new NotFoundError(`User with ID ${id} not found.`);
        }

        const { email, firstName, isActive, lastName, username, role } = request.body;

        if (email) {
            user.email = email;
        }

        if (firstName) {
            user.firstName = firstName;
        }

        if (isActive) {
            user.isActive = isActive;
        }

        if (lastName) {
            user.lastName = lastName;
        }

        if (username) {
            user.username = username;
        }

        if (role) {
            user.role = role;
        }

        try {
            await user.save();
        } catch (editError) {
            const error = editError as Error.ValidationError;

            throw new ClientError(processErrors(error));
        }

        response.status(201).type('json').send(user.toJSON());
    };

    static deleteUser = async (request: Request, response: Response) => {
        const id: string = request.params.id;
        const user = await User.findById(id).exec();

        if (!user) {
            throw new NotFoundError(`User with ID ${id} not found.`);
        }

        await user.deleteOne();
        response.status(204).type('json').send({ message: "User deleted successfully." });
    };
}

export default UserController;
