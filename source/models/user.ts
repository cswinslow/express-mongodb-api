import bcrypt from 'bcrypt';
import { Document, Model, model, Schema } from 'mongoose';
import { isEmail } from 'validator';

import { USER_ROLES } from '../utilities/constants';

export interface IUser extends Document {
    email: string,
    firstName: string,
    isActive: boolean,
    lastName: string,
    password: string,
    role: string,
    username: string
};

export interface IUserMethods {
    toJSON(): IUser;
    validateCredentials(password: string): Promise<boolean>;
};

interface UserModel extends Model<IUser, {}, IUserMethods> {
    buildUser(attribute: IUser): IUser;
};

const userSchema = new Schema<IUser, UserModel, IUserMethods>({
    email: {
        type: String,
        unique: true,
        validate: [ isEmail, 'Invalid email address.' ]
    },
    firstName: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        minLength: [6, 'Password is too short.'],
        maxLength: [42, 'Password is too long.']
    },
    role: {
        type: String,
        required: true,
        enum: [USER_ROLES.ADMINISTRATOR, USER_ROLES.AGENT, USER_ROLES.DEVELOPER, USER_ROLES.USER],
        default: USER_ROLES.USER
    },
    username: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true,
        minLength: [4, 'Username is too short.'],
        maxLength: [18, 'Username is too long.'],
        validate: {
            validator: async function(value: string): Promise<boolean> {
                let document: any = await User.findOne({ username: value });

                if (document) {
                    // @ts-ignore
                    return this._id.toString() == document._id.toString();
                } else {
                    return Boolean(!document);
                }
            },
            message: 'Username is already in use.'
        }
    }
}, {
    timestamps: true
});

userSchema.pre('save', async function(next) {
    if (!this.isModified('password') || !this.password) {
        return next();
    }

    this.password = await bcrypt.hash(this.password, 12);

    next();
});

userSchema.methods.toJSON = function() {
    const user = this as IUser;
    const userObject = user.toObject();

    delete userObject.__v;
    delete userObject.password;

    return userObject;
};

userSchema.methods.validateCredentials = async function(password: string): Promise<boolean> {
    return await bcrypt.compare(password, this.password);
};

userSchema.statics.buildUser = (attribute: IUser) => {
    return new User(attribute);
};

const User = model<IUser, UserModel>('User', userSchema);

export { User };
