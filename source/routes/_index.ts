import { Router } from 'express';

import authentication from './authentication';
import user from './user';

const routes = Router();

routes.use('/authentication', authentication);
routes.use('/users', user);

export default routes;
