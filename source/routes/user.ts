import { Router } from 'express';

import { asyncHandler } from '../middleware/asyncHandler';
import { checkRoleMiddleware, verifyTokenMiddleware } from '../middleware/authorization';

import { USER_ROLES } from '../utilities/constants';

import UserController from '../controllers/UserController';

const router = Router();

router.get(
    '/',
    [verifyTokenMiddleware, checkRoleMiddleware([USER_ROLES.ADMINISTRATOR, USER_ROLES.AGENT, USER_ROLES.DEVELOPER])],
    asyncHandler(UserController.getAll)
);

router.get(
    '/:id([0-9a-z]{24})',
    [verifyTokenMiddleware, checkRoleMiddleware([USER_ROLES.ADMINISTRATOR, USER_ROLES.AGENT, USER_ROLES.DEVELOPER])],
    asyncHandler(UserController.getOne)
);

router.post(
    '/',
    asyncHandler(UserController.createUser)
);

router.patch(
    '/:id([0-9a-z]{24})',
    [verifyTokenMiddleware, checkRoleMiddleware([USER_ROLES.ADMINISTRATOR, USER_ROLES.AGENT, USER_ROLES.DEVELOPER])],
    asyncHandler(UserController.editUser)
);

router.delete(
    '/:id([0-9a-z]{24})',
    [verifyTokenMiddleware, checkRoleMiddleware([USER_ROLES.ADMINISTRATOR, USER_ROLES.AGENT, USER_ROLES.DEVELOPER])],
    asyncHandler(UserController.deleteUser)
);

export default router;
