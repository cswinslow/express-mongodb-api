import { Router } from 'express';

import { asyncHandler } from '../middleware/asyncHandler';
import { verifyTokenMiddleware } from '../middleware/authorization';

import AuthenticationController from '../controllers/AuthenticationController';

const router = Router();

router.post(
    '/login',
    asyncHandler(AuthenticationController.login)
);

router.post(
    '/refresh',
    asyncHandler(AuthenticationController.refreshAccessToken)
);

router.post(
    '/change-password',
    [verifyTokenMiddleware],
    asyncHandler(AuthenticationController.changePassword)
);

export default router;
