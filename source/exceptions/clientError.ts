import { CustomError } from '../utilities/customError';

export class ClientError extends CustomError {
    constructor (message: string) {
        super(message, 400);
    }
};
