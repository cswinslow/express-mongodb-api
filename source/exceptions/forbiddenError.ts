import { CustomError } from '../utilities/customError';

export class ForbiddenError extends CustomError {
    constructor (message: string) {
        super(message, 403);
    }
};
