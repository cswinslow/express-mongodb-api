import express, { json, urlencoded } from 'express';
import mongoose from 'mongoose';

import configuration from './configuration';

import { errorHandler } from './middleware/errorHandler';

import routes from './routes/_index';

const server = express();

server.use(json());
server.use(urlencoded({ extended: true }));

server.use('/' + configuration.prefix + '/' + configuration.version, routes);

server.use(errorHandler);

mongoose
    .connect(configuration.databaseUri!)
    .then((response) => {
        console.log("Established initial connection to database.");

        server.listen(configuration.port, () => {
            console.log(`Server is listening on port ${configuration.port}.`);
        });
    })
    .catch((error) => {
        console.log("Initial database connection error occured:", error);
    });
