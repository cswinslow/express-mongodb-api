import { Response } from 'express';

import { CustomError, ICustomError } from '../utilities/customError';

export function errorHandler(error: any, response: Response) {
    console.error(error);

    if (!(error instanceof CustomError)) {
        response.status(500).send(JSON.stringify({
            message: "Server error, please try again later."
        }));
    } else {
        const customError = error as CustomError;
        let errorResponse = {
            message: customError.message
        } as ICustomError;

        if (customError.additionalInformation) {
            errorResponse.additionalInformation = customError.additionalInformation;
        }

        response.status(customError.status).type('json').send(JSON.stringify(errorResponse));
    }
};
