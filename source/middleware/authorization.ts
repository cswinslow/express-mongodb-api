import { Request, Response, NextFunction } from 'express';
import { JwtPayload, verify } from 'jsonwebtoken';

import configuration from '../configuration';

import { User } from '../models/user';

export interface CustomRequest extends Request {
    token: JwtPayload;
}

export const checkRoleMiddleware = (roles: Array<string>) => {
    return async (request: Request, response: Response, next: NextFunction) => {
        const user = await User.findById((request as CustomRequest).token.payload.userId);

        if (!user) {
            response.status(404)
                .type('json')
                .send(JSON.stringify({ message: "User not found." }));

            return;
        }

        if (roles.indexOf(user.role) > -1) {
            next();
        } else {
            response.status(403)
                .type('json')
                .send(JSON.stringify({ message: "Invalid permissions." }));

            return;
        }
    };
};

export const verifyTokenMiddleware = (request: Request, response: Response, next: NextFunction) => {
    const token = <string>request.headers['authorization'];
    let tokenPayload;

    try {
        tokenPayload = <any>verify(token?.split(' ')[1], configuration.jwt.secret!, {
            complete: true,
            audience: configuration.jwt.audience,
            issuer: configuration.jwt.issuer,
            algorithms: ['HS256'],
            clockTolerance: 0,
            ignoreExpiration: false,
            ignoreNotBefore: false
        });

        (request as CustomRequest).token = tokenPayload;
    } catch (error) {
        response.status(401)
            .type('json')
            .send(JSON.stringify({ message: "Missing or invalid token." }));

        return;
    }

    next();
};
