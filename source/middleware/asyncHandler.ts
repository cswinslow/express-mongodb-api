import { Request, Response, NextFunction } from 'express';

export const asyncHandler = (func: (request: Request, response: Response, next: NextFunction) => void) => (request: Request, response: Response, next: NextFunction) => {
    return Promise.resolve(func(request, response, next)).catch(next);
};
