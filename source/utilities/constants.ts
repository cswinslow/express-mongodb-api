export const USER_ROLES = {
    ADMINISTRATOR: 'Administrator',
    AGENT: 'Agent',
    DEVELOPER: 'Developer',
    USER: 'User'
};
