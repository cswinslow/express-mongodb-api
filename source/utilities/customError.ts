export class CustomError extends Error {
    message!: string;
    status!: number;
    additionalInformation!: any;

    constructor (message: string, status: number = 500, additionalInformation: any = undefined) {
        super(message);

        this.message = message;
        this.status = status;
        this.additionalInformation = additionalInformation;
    };
};

export interface ICustomError {
    message: string;
    additionalInformation?: string;
};
