import * as dotenv from 'dotenv';

dotenv.config();

const configuration = {
    databaseUri: process.env.DATABASE_URI,
    jwt: {
        audience: process.env.JWT_AUDIENCE,
        issuer: process.env.JWT_ISSUER,
        secret: process.env.JWT_SECRET
    },
    port: process.env.SERVER_PORT,
    prefix: process.env.API_PREFIX,
    url: process.env.SERVER_URL,
    version: process.env.API_VERSION
};

export default configuration;
