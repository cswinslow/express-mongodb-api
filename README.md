# Express MongoDB API
Basic implementation of a RESTful API utilizing Express for the server, MongoDB for the database, and JWT for authentication.
